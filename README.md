### 宽度
- `w1 ~ w10` 可以快速设置元素的宽度为10% ~ 100%
- `w10` 设置元素宽度为100%（实际中不常用）
- `wp10` 可以快速设置元素的宽度为10px~
### 高度
- `h5 h10` 可以快速设置元素的高度为50%  100%
- `hp10` 可以快速设置元素的高度为10px~
- `hp10` 可以快速设置元素的高度为10px~

### 定位
- `pos-fmt` 格式化为绝对定位，并且生成BFC，使得当前元素填充满其`offsetParent`元素,

- `pos-ct` 水平居中（注：父元素要为非static定位）
- `pos-md` 垂直居中（注：父元素要为非static定位）
- `pos-mc` 垂直+水平居中（注：父元素要为非static定位）


### 弹性盒子
- `flex` 设置元素为弹性盒子
- `flex-md` 等效于 align-items: center;
- `flex-ct` 等效于 justify-content: center;
- `flex-dir-column` 等效于 flex-direction: column;
- `flex-sb` 等效于 justify-content: space-between;
- `flex-sa` 等效于 justify-content: space-around;
- `flex-mc` 同时设置 align-items: center;justify-content: center;
- `flex-item-1 ~ flex-item-4` 设置flex属性为1~4


### 边距
- `pd5、pd10、pd15` 设置左右的内边距
- `mg5、mg10、mg15` 设置上下的外边距


### 颜色
- `bg-指定颜色名称` 设置背景颜色
- `color-指定颜色名称` 设置文字颜色

### 文字
- `txt-ct` 居中
- `txt-left` 靠左
- `txt-right` 靠右
- `txt-justify` 两端对齐
- `single-text-overflow` 单行超出隐藏
- `multiple-text-overflow` 多行超出隐藏（2行）
- `font12 ~ font30` 设置12 ~ 30 之间的常用字体大小
- `font-wb` 加粗
- `font-wn` 设为常规字体
